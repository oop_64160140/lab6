package com.panneerat.week6;

public class BookBankApp {
     public static void main(String[] args) {
        BookBank panneerat = new BookBank("Panneerat", 100.0); //Default Contructor
        panneerat.print();
        panneerat.deposit(50);
        panneerat.print();
        panneerat.withdraw(50);
        panneerat.print();

        BookBank prayood = new BookBank("Prayood", 1);
        prayood.deposit(100000);
        prayood.withdraw(10000000);
        prayood.print();

        BookBank praweet = new BookBank("Praweet", 10);
        praweet.deposit(10000000);
        praweet.withdraw(1000000);
        praweet.print();
    }
}
